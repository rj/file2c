No dependencies, beyond the standard C library, are required to build
a copy of file2c.  The application can be built and installed using
the standard commands, as follows:

    ./configure
    make
    make install

Thank-you for using file2c.

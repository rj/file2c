package main

import (
	"fmt"
	"io"
	"math/rand"
	"os"
	"strconv"
)

// must panics if the error is not nil.
// This is just a short script, so we are going to forego proper error handling.
func must(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	size, err := strconv.ParseUint(os.Args[1], 10, 64)
	must(err)
	if size > 40 {
		fmt.Println("Output file size is too big.  Let's not blow up the computer.")
		os.Exit(1)
	}
	size = 1 << size
	fmt.Println("Size: ", size)

	out, err := os.OpenFile(os.Args[2], os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0666)
	must(err)

	in := &io.LimitedReader{
		R: rand.New(rand.NewSource(7)),
		N: int64(size),
	}
	n, err := io.Copy(out, in)
	must(err)
	if size != uint64(n) {
		panic("Internal error")
	}
}

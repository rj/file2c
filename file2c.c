/*
** This code is distributed under a BSD license.  Please refer to the
** accompanying LICENSE.txt.
**
*************************************************************************
**
** Copyright (c) 2010 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains the entire implementation of the command-line
** application 'file2c'.  The purpose of this program is to convert any
** file into C source code that contains a statically initialized
** variable, whose contents is the original source file.
**
*/

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char const* in_filename = 0;
char const* out_filename = 0;
char const* var_name = 0;
int var_size = 1;

#define LINE_LENGTH_LIMIT 60

static void escape_copy( FILE* out, FILE* in )
{
  int c;
  unsigned col = 0;

  assert( out );
  assert( in );

  /* This loop handles printable characters */
  /* Make use of escape sequences wherever possible */
  /* Want to avoid switching into escaped hexadecimal notation */
  while ( ( c = fgetc( in ) ) != EOF ) {
    switch ( c ) {
    case '\"':
      fputs( "\\\"", out );
      col += 2;
      break;

    case '\\':
      fputs( "\\\\", out );
      col += 4;
      break;

    case '\b':
      fputs( "\\b", out );
      col += 2;
      break;

    case '\t':
      fputs( "\\t", out );
      col += 2;
      break;

    case '\n':
      fputs( "\\n", out );
      col += 2;
      break;

    case '\v':
      fputs( "\\v", out );
      col += 2;
      break;

    case '\r':
      fputs( "\\r", out );
      col += 2;
      break;

    case '\a':
      fputs( "\\a", out );
      col += 2;
      break;

    default:
      if ( isprint( c ) ) {
        fputc( c, out );
        col++;
      } else {
        /* Going to need to switch to hexadecimal notation */
        /* Print out the character */
        fprintf( out, "\\x%x", (unsigned)c );
        col += 3;
        /* Switch to binary data loop */
        goto binarydata;
      }
    }

  textdata:
    if ( col > LINE_LENGTH_LIMIT ) {
      fprintf( out, "\"\n\"" );
      col = 0;
    }
  }
  return;

  /* This loop handles binary data */
  /* Move back to printing character data, unless it would
     cause some ambiguity in the interpretation of the hex
     digit. */
  while ( ( c = fgetc( in ) ) != EOF ) {
    /* back slash is printable with an escape sequence */
    if ( c == '\\' ) {
      fputs( "\\\\", out );
      col += 4;
      goto textdata;
    }
    if ( isprint( c ) && !isxdigit( c ) && c != '"' ) {
      fputc( c, out );
      col++;
      goto textdata;
    } else {
      fprintf( out, "\\x%x", (unsigned)c );
      col += 3;
    }

  binarydata:
    if ( col > LINE_LENGTH_LIMIT ) {
      fprintf( out, "\"\n\"" );
      col = 0;
    }
  }
  return;
}

void print_usage( FILE* out )
{
  fprintf( out,
           "Usage:\n"
           "\tfile2c <input file> <output file> <variable name>\n" );
}

int main( int argc, char* argv[] )
{
  FILE* in = 0;
  FILE* out = 0;

  if ( argc == 2 && strcmp( argv[1], "-h" ) == 0 ) {
    print_usage( stdout );
    return EXIT_SUCCESS;
  }

  if ( argc != 4 ) {
    fprintf( stderr, "error:  incorrect use\n\n" );
    print_usage( stderr );
    return EXIT_FAILURE;
  }

  in_filename = argv[1];
  out_filename = argv[2];
  var_name = argv[3];

  in = fopen( in_filename, "rt" );
  if ( !in ) {
    fprintf(
        stderr, "Could not open %s\n%s\n", in_filename, strerror( errno ) );
    return EXIT_FAILURE;
  }

  out = fopen( out_filename, "wt" );
  if ( !out ) {
    fprintf(
        stderr, "Could not open %s\n%s\n", out_filename, strerror( errno ) );
    return EXIT_FAILURE;
  }

  fprintf( out, "#include <stddef.h>\n" );
  fprintf( out, "char const %s[] = \n\"", var_name );
  escape_copy( out, in );
  fprintf( out, "\";\n" );
  if ( var_size ) {
    fprintf( out, "size_t const %s_size = sizeof(%s);\n", var_name, var_name );
  }

  fclose( in );
  fclose( out );

  return EXIT_SUCCESS;
}
